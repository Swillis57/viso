﻿using UnityEngine;
using System.Collections.Generic;

//An asteroid orbits around the Centroid and follows it as it crosses the system
public class Asteroid : Orbiter 
{
	public Orbiter Cluster;

	public override void Start () 
	{
		base.Start();
		_enable3d = 1; //Asteroids orbit in a sphere rather than a circle

	}
	
	// Update is called once per frame
	public override void Update () 
	{
		base.Update ();
	}

	protected override void Steer()
	{
		Vector3 force = Vector3.zero;

		force += CalcFlocking();

		force += CalcNewtonOrbit() * AstOrbWt;

		if ((Centroid.transform.position - transform.position).sqrMagnitude < Mathf.Pow(Centroid.transform.localScale.x/2, 2))
		{
			Cluster.OrbitingBodies.Remove(gameObject);
			Destroy(gameObject);
			return;
		}

		AddForce (force);
		_a.Normalize();
		_a *= MaxForce;
	}

	//Calculates all flocking forces in one function call
	protected Vector3 CalcFlocking()
	{
		//Separation and alignment
		Vector3 sum = Vector3.zero;
		Vector3 avgV = Vector3.zero;
		foreach (GameObject b in Cluster.OrbitingBodies)
		{
			if (b == gameObject || (b.transform.position - transform.position).sqrMagnitude > Mathf.Pow(AstSepDist, 2)) continue;
			
			sum += -Seek (b.transform.position) / ((b.transform.position - transform.position).sqrMagnitude + 0.001f);
			
			avgV += b.transform.forward;
		}
		sum *= AstSepWt;
		
		avgV.Normalize();
		avgV *= MaxSpeed;
		avgV.y *= _enable3d;
		sum += Seek(avgV) * AstAlgnWt;
		
		//Cohesion
		sum += Seek(transform.position) * AstChsWt;
		
		//Wander
		Vector3 t = transform.position + transform.forward * AstWndrDist;
		Vector3 off = Quaternion.Euler(0, AstWndrAng, 0) * transform.forward;
		t += off * AstWndrRad;
		AstWndrAng += Random.Range(-1, 1);
		sum += Seek(t) * AstWndrWt;
		
		return sum;
	}

	//Sets the centroid and cluster
	//Needed more generic behavior rather than setting these to the last cluster created
	public void SetCentroid(GameObject g)
	{
		Centroid = g;
		Cluster = g.GetComponent<Orbiter>();
	}

	//Sets up initial acceleration and velocity vectors for orbiting a planet
	public void SetUpInitVectors(GameObject cent)
	{
		Centroid = cent;
		transform.forward = Vector3.Cross(transform.position - Centroid.transform.position, Vector3.up).normalized;
		MaxSpeed = transform.position.magnitude/1.25f;
		MaxForce = MaxSpeed *0.8f; 

		_a = transform.forward * MaxForce * (transform.position - Centroid.transform.position).magnitude/1100f;
		_v = transform.forward * MaxSpeed * (transform.position - Centroid.transform.position).magnitude/1100f;

	}
}
