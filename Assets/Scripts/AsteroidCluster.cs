﻿using UnityEngine;
using System.Collections.Generic;

//Acts as the centroid for a group of asteroids
public class AsteroidCluster : Orbiter
{
	public int AsteroidsPerCluster = 10;
	public int ClusterRadius = 1;


	private Vector3 _start, _end;

	public override void Start()
	{
		base.Start();

		//Create starting position on the unit sphere
		//Then get the end point by reflecting start and rotating it by a random angle
		_start = GameObject.FindGameObjectWithTag("MainCamera").transform.position;
		_start.y *= _enable3d;
		_end = Quaternion.Euler(0, Random.Range(-30, 30), 0) * -_start;
		_end.y *= _enable3d;
		transform.position = _start;


		//Generate asteroids for this cluster
		for(int i = 0; i < AsteroidsPerCluster; i++)
		{
			OrbitingBodies.Add((GameObject)GameObject.Instantiate(_sceneController.AsteroidObject));
			OrbitingBodies[i].transform.position = Random.insideUnitSphere * ClusterRadius + _start;
			OrbitingBodies[i].transform.localScale = new Vector3(3, 3, 3);
			OrbitingBodies[i].GetComponent<Asteroid>().Mass = 150000;
			OrbitingBodies[i].GetComponent<Asteroid>().SetCentroid(gameObject);
		}
	}


	public override void Update()
	{
		base.Update();
		Debug.DrawLine(transform.position, _end);

		//Check for collision with a planet
		//Can't be checked per-asteroid; way too slow
		foreach(GameObject g in _sceneController.Planets)
		{
			if ((g.transform.position - transform.position).sqrMagnitude < Mathf.Pow(g.transform.localScale.x/2, 2))
			{
				foreach(GameObject a in OrbitingBodies) Destroy(a);
				Destroy(gameObject);
			}
		}

		//Check for collision with sun or reaching the destination 
		if ((_end - transform.position).sqrMagnitude < 100 || transform.position.sqrMagnitude < Mathf.Pow(100, 2)) 
		{
			foreach(GameObject g in OrbitingBodies) Destroy(g);
			Destroy(gameObject);
		}

	}

	//The cluster moves across the system, and its local asteroids follow it
	protected override void Steer()
	{
		Vector3 force = Vector3.zero;

		force += Seek (_end);

		AddForce(force);
		_a.Normalize();
		_a *= MaxForce;

	}

	protected Vector3 Seek(GameObject f, GameObject t)
	{
		Vector3 dv = t.transform.position - f.transform.position;
		
		dv.Normalize ();
		dv *= MaxSpeed;
		Vector3 steer = dv - _v;
		steer.y *= _enable3d;
		
		return steer; 
	}


}
