﻿using UnityEngine;
using System.Collections.Generic;
  
public class Planet : Orbiter
{
	private float _initRadius;

	// Use this for initialization
	public override void Start()
	{
		base.Start();
		Vector3 pos = Random.insideUnitSphere * _sceneController.SystemRadius;
		pos.y *= _enable3d;
		_initRadius = pos.magnitude;
		float r = Random.Range(10, 40) * 3;
		transform.position = pos;
		transform.localScale = new Vector3(r, r, r);

		//Calculates mass based on average planet density and distance from the star
		Mass = (4/3 * Mathf.Pow(r, 3)) * 5.5f*ONE_AU/(pos.magnitude); 

		float relDist = pos.magnitude/_sceneController.SystemRadius;

		GetComponent<MeshRenderer>().materials[0].mainTexture = _sceneController.PlanetTextures[
		    Random.Range(0, 6)
		];
		
		//Generate the forward vector in the direction of rotation
		//And calculate the MaxSpeed and MaxForce needed to keep the planet in orbit
		transform.forward = Vector3.Cross(transform.position - Centroid.transform.position, Vector3.up).normalized;
		MaxSpeed = transform.position.magnitude/1.25f;
		MaxForce = MaxSpeed *0.8f; 

		//Set initial acceleration and velocity so the planet doesn't fall into the sun
		//This is what allows the planet to stay in orbit while only calculating the inwards gravitational vector
		_a = transform.forward * MaxForce * (transform.position - Centroid.transform.position).magnitude/1100f;
		_v = transform.forward * MaxSpeed * (transform.position - Centroid.transform.position).magnitude/1100f;

		//Create moons for the planet
		for(int i = 0; i < Random.Range(3, 8); i++)
		{
			GameObject g = (GameObject)GameObject.Instantiate(_sceneController.AsteroidObject);
			g.transform.parent = transform;
			g.transform.position = Random.insideUnitSphere * 300+transform.position;
			g.transform.localScale = new Vector3(0.25f, 0.25f, 0.25f);
			g.GetComponent<Orbiter>().Mass = 150000;
			g.GetComponent<Asteroid>().SetCentroid(gameObject);
			g.GetComponent<Asteroid>().SetUpInitVectors(gameObject);
			OrbitingBodies.Add(g);

		}
	}

	// Update is called once per frame
	public override void Update()
	{
		base.Update();
		
		Debug.DrawLine(transform.position, Centroid.transform.position);

		//Check if planet has hit the sun
		//Some orbits are inherently unstable, so they will always fall into the sun
		DistFromCentroid = (Centroid.transform.position - transform.position).sqrMagnitude;
		if (DistFromCentroid < Mathf.Pow(Centroid.transform.localScale.x/2 + transform.localScale.x/2, 2))
		{
			_sceneController.Planets.Remove(gameObject);
			Destroy (gameObject);
		}
	}
	
	protected override void Steer()
	{
		Vector3 force = Vector3.zero;
		
		force += OrbWt * CalcNewtonOrbit();

		//Add a small separation force to spread the planets out a little
		force += SepWt * CalcSeparation(_sceneController.Planets);
		
		AddForce(force);
		_a.Normalize();
		_a *= MaxForce;
	}



}
