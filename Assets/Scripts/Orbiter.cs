using UnityEngine;
using System.Collections.Generic;

//Represents an orbiting body
public abstract class Orbiter : MonoBehaviour
{
	public const float OrbWt = 100.0f;
	public const float SepWt = 300.0f;
	public const float PathWt = 100.0f;
	public const float G = 6.67384f;
	public const float ONE_AU = 300;
	
	public int AstSepWt = 110;
	public int AstSepDist = 20;
	public int AstChsWt = 100;
	public int AstOrbWt = 150;
	public int AstAlgnWt = 80;
	public int AstWndrWt = 70;
	public int AstWndrDist = 50;
	public int AstWndrAng = 0;
	public int AstWndrRad = 30;
	
	public float MaxSpeed = 50.0f;
	public float MaxForce = 20.0f;
	public float Mass;
	public float DistFromCentroid;
	public GameObject Centroid;
	public float CentroidMass;
	public List<GameObject> OrbitingBodies;

	protected Vector3 _a, _v;
	protected SceneController _sceneController;
	protected int _enable3d;

	abstract protected void Steer ();

	virtual public void Start()
	{ 
		_a = Vector3.zero;
		_v = Vector3.zero;
		_sceneController = GameObject.FindGameObjectWithTag("ScriptObject").GetComponent<SceneController>();
		_enable3d = _sceneController.Enable3DMotion;
		OrbitingBodies = new List<GameObject>();
	}

	virtual public void Update()
	{
		//If the thing we're orbiting is gone, we can't really orbit it anymore
		if (Centroid == null) Destroy(gameObject);
		Steer();

		_v += _a * Time.deltaTime;
		_v.y *= _enable3d;
		_v = Vector3.ClampMagnitude(_v, MaxSpeed);

		transform.position += _v * Time.deltaTime;

		Debug.DrawRay(transform.position, _v);

		_a = Vector3.zero;
		transform.forward = _v.normalized; 

	}

	virtual protected Vector3 Seek (Vector3 targetPos)
	{
		Vector3 dv = targetPos - gameObject.transform.position;

		dv.Normalize ();
		dv *= MaxSpeed;
		Vector3 steer = dv - _v;
		steer.y *= _enable3d;

		return steer; 
	}

	//Calculates the inward force using Newton's Law of Gravitation
	protected Vector3 CalcNewtonOrbit()
	{
		Vector3 sum = GravForce (Mass, CentroidMass, Centroid.transform.position);

		sum.y *= _enable3d;
		return sum;
	}

	protected Vector3 CalcNewtonOrbit(Orbiter o)
	{
		Vector3 sum = GravForce (Mass, o.Mass, o.gameObject.transform.position);
		
		sum.y *= _enable3d;
		return sum;
	}

	//Returns a force vector towards the centroid 
	virtual protected Vector3 GravForce(float m1, float m2, Vector3 tar)
	{
		//Uses the vector-based gravitation formula
		return G * m1 * m2 * (tar - transform.position) / ((tar - transform.position).sqrMagnitude+0.001f);
	}

	//Standard separation force calculation
	protected Vector3 CalcSeparation(List<GameObject> bodies)
	{
		Vector3 sum = Vector3.zero;
		foreach (GameObject g in bodies)
		{
			if (g == gameObject) continue; //Don't divide by zero
			sum += Seek (g.transform.position) / (g.transform.position - transform.position).sqrMagnitude;
		}
		return new Vector3(-sum.z, 0, sum.x);
	}


	protected void AddForce (Vector3 f)
	{
		_a += f / Mass;
	}

}
