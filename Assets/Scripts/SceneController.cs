﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class SceneController : MonoBehaviour 
{
	public GameObject Star;
	public GameObject AsteroidObject;
	public GameObject PlanetObject;
	public GameObject AsteroidClusterObject;

	public int NumPlanets;
	public int StarMass = 1000000;
	public int Enable3DMotion = 0;
	public int CurrNumBodies;
	public int SystemRadius;
	public int SelectedBody;
	public List<GameObject> Planets;
	public List<GameObject> Clusters;
	public Texture2D[] PlanetTextures;

	private System.Random _rnd;
	private Vector3 _starCenter;
	private GameObject _cameraAnchor;
	private List<string> _gridStrings;

	void Start () 
	{
		_starCenter = new Vector3(500, 0, 500);
		_cameraAnchor = GameObject.FindGameObjectWithTag("CameraAnchor");
		_gridStrings = new List<string>();

		UnityEngine.Random.seed = (new DateTime(1970, 1, 1) - DateTime.UtcNow).Seconds; 
		SelectedBody = 0;
		Clusters = new List<GameObject>();
		Planets = new List<GameObject>();

		for(int i = 0; i < NumPlanets; i++)
		{
			Planets.Add((GameObject)GameObject.Instantiate(PlanetObject));
			Planets[i].GetComponent<Orbiter>().Centroid = Star;
			Planets[i].GetComponent<Orbiter>().CentroidMass = StarMass;

		}
	}

	//Draw the column of buttons to switch planet views
	void OnGUI()
	{
		_gridStrings.Clear();
		_gridStrings.Add("System View");
		for(int i = 1; i <= Planets.Count; i++)
		{
			_gridStrings.Add("Planet " + (i));
		}


		SelectedBody = GUI.SelectionGrid(new Rect(25, 25, 100, _gridStrings.Count*30), SelectedBody, _gridStrings.ToArray(), 1);
	}

	void Update () 
	{
		CurrNumBodies = Planets.Count;

		//If system view is selected, reset the camera anchor position
		if (SelectedBody == 0)
		{
			_cameraAnchor.transform.parent = null;
			_cameraAnchor.transform.position = Vector3.zero;
		}

		//If one of the planets is selected, move the anchor to that planet and set the parent transform
		//for smooth movement
		if(SelectedBody >= 1 && SelectedBody <= Planets.Count)
		{
			_cameraAnchor.transform.position = Planets[SelectedBody-1].transform.position;
			_cameraAnchor.transform.parent = Planets[SelectedBody-1].transform;
		}

		//Asteroid clusters are spawned on a keypress
		if(Input.GetKeyUp(KeyCode.S))
		{
			Clusters.Add((GameObject)GameObject.Instantiate(AsteroidClusterObject));
		}

		//Mousewheel can be used to zoom
		if (Input.GetAxis("Mouse ScrollWheel") > 0)
		{
			Camera.main.transform.position += 50*Camera.main.transform.forward;
		}

		if (Input.GetAxis("Mouse ScrollWheel") < 0)
		{
			Camera.main.transform.position -= 50*Camera.main.transform.forward;
		}
	}


}
