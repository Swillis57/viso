﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

	public float LookSpeedX = 1f;
	public float LookSpeedY = 1f;

	private GameObject _camera;	
	private Vector3 _prevMsState, _currMsState;

	void Start () {
		_prevMsState = Vector3.zero;
		_currMsState = Input.mousePosition;
		_camera = GameObject.FindGameObjectWithTag("MainCamera"); 

		/*if (GameObject.FindGameObjectWithTag("ScriptObject").GetComponent<SceneController>().Enable3DMotion == 1)
		{
			_camera.transform.rotation = Quaternion.Euler(45, 180, 0);
			_camera.transform.position = new Vector3(0, 1200, 1200);
		}
		else
		{
			_camera.transform.rotation = Quaternion.Euler(0, 180, 0);
			_camera.transform.position = new Vector3(0, 1200, 0);
		}*/
	}

	void Update () {
		_currMsState = Input.mousePosition;

		//if LMB is held and the mouse was moved, rotate the camera by that angle
		if (Input.GetMouseButton(0) && !_prevMsState.Equals(_currMsState))
		{
			float dx = (_currMsState.x - _prevMsState.x) * LookSpeedX;
			float dy = (_currMsState.y - _prevMsState.y) * LookSpeedY;


			Quaternion rot = Quaternion.AngleAxis(dx, Vector3.up) * transform.rotation;
			rot.z = 0;
			transform.rotation = rot;
		}

		_prevMsState = _currMsState;


	}
}
